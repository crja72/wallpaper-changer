import ctypes
import datetime
from pathlib import Path


def main():
    path = f"{str(Path.home())}\OneDrive\Изображения\wallpapers\{datetime.datetime.now().month:02d}.png"
    ctypes.windll.user32.SystemParametersInfoW(20, 0, path, 3)


if __name__ == '__main__':
    main()
